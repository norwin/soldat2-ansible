# Soldat 2 dedicated server on hetzner.cloud via Ansible

This Ansible playbook can be used to deploy a Soldat 2 dedicated server on an adhoc virtual machine at Hetzner.
Keep in mind that you need to pay for the virtual machine at Hetzner ([per hour](https://www.hetzner.com/cloud)).

## Prerequisites

- basic knowledge of linux terminal usage
- Hetzner account with a valid creditcard / SEPA
    - Hetzner api token
- a workstation with the following software installed
    - ansible
    - hetzner.hcloud ansible plugin (`ansible-galaxy collection install hetzner.hcloud`)
    - pip 3
    - git
- a SSH keypair

## Getting Started

1. Get this playbook:
    ```sh
    git clone https://codeberg.org/norwin/soldat2-ansible
    cd soldat2-ansible
    ```
2. Get an hetzner.cloud API token at <https://console.hetzner.cloud> (`https://console.hetzner.cloud/projects/<id>/security/tokens`), and store it in environment:
    ```sh
    export HCLOUD_TOKEN=<myapikey>
    ```
3. Configure the server(s).
    `inventory.yml` defines hosts that will be provisioned with a server instance each. You can override server variables per host, see [configuration](#configuration):
    ```sh
    $EDITOR inventory.yml # edit list of servers to provision
    ```
4. Provision & start the server(s):
    ```sh
    ansible-playbook -i inventory.yml play.yml -t provision
    ```
5. Stop the server(s) once done:
    ```sh
    ansible-playbook -i inventory.yml play.yml -t deprovision
    ```

## Configuration
The following variables are available:

| required | role    | name                      | default | description |
|----------|---------|---------------------------|---------|-------------|
| true     | hetzner | `hetzner_ssh_keys`        |         | dict of keyname-pubkey pairs |
|          | hetzner | `hetzner_server_type`     | `cx11`  | 1 vCPU, 1 GB RAM |
|          | hetzner | `hetzner_server_location` | `fsn1`  | [available values](https://docs.hetzner.com/cloud/general/locations) |
|          | soldat2 | `soldat_servername`       | value in soldat2 config, by default `Soldat 2 Dedicated Server` | |
|          | soldat2 | `soldat_serverpassword`   |         |             |
|          | soldat2 | `soldat_admin_playfab`    |         | player ID to grant admin rights to |
|          | soldat2 | `soldat_config_repo`      | [`https://codeberg.org/norwin/soldat2-custom`](https://codeberg.org/norwin/soldat2-custom) | URL to a repo containing custom server configuration(s) |
|          | soldat2 | `soldat_config_version`   |         | when set to a commit or branch name, will apply a server configuration from `soldat_config_repo` |

To apply custom Soldat server configuration, create a similarly structured repo as <https://codeberg.org/norwin/soldat2-custom> and define its URL in `soldat_config_repo`.

## Advanced usage: tags

Subsets of actions can be selected via tags (`ansible-playbook --tags`) - by default the playbook executes the full lifecycle of the VPS including deprovisioning.

| tag            | purpose                        | steps                                            |
|----------------|--------------------------------|--------------------------------------------------|
| provision      | create server                  | provision VPS, install & (re)start soldat server |
| deprovision    | delete all defined VPSs        | deprovision VPS                                  |
| reload         | apply updates in config repo   | pull config updates, restart soldat2 service     |
| hetzner        | test provisioning              | provision VPS, deprovision VPS                   |

## TODOs

- [ ] add fast playbooks (tags?) for restarting, stopping servers, updating config
- [ ] deploy multiple server instances per host

## License
© @noerw, licensed und GPL-3-or-later
